*This project is no longer actively maintained or supported! I simply don't have the time or energy for it anymore (and haven't had for a while). The code is free to use and fork into other projects, so please don't hesitate to create something awesome with it if you like :).*
# - - -

# WhoCalled

Miss the function lookup from Eclipse? Still using ctrl+shift+f to find function definitions and functions calls in your projects? WhoCalled is here to help.

All OS:s supported!

Works for both Sublime Text 2 & 3!

Installation
------------

**Package Control**  
Easiest and quickest way to install is through Package Control:  
[http://wbond.net/sublime\_packages/package\_control](http://wbond.net/sublime\_packages/package\_control)

Overview
--------

WhoCalled lets you find function definitions and function calls in project folders with a quick keyboard short cut. It does so by looking for the function in pre-complied JSON index files for the corresponding project folder that you are currently viewing a file in. The indexer is run silently and automatically in a separate thread every time Sublime Text is started or manually whenever you suspect that a re-indexing is necessary. WhoCalled will also silently re-index the file you are currently working on every time you save it.

In short, hitting the short cut keys for function definitions will show you all corresponding function definitions in that project folder. Similarly, hitting the short cut keys for function calls will show you all corresponding function calls.

#### What it does:
* Looks up function definitions and function calls per user defined project folder.
* Indexes all functions in the background, so you don't have to worry about hickups.
* Automatic indexing on Sublime Text startup, updating all changed files.
* On-the-fly indexing on file save, keeping your recently edited files fresh.

#### What it is:
* Quite fast.
* Nonintrusive.
* Easy to setup and use.
* A complement to the built-in function lookup tool.
* Awesome, most of the time.

#### What it isn't:
* Fond of very very very large project folders.
* Bug free.
* Completely stable.
* Completely reliable.
* A replacement for the built-in function lookup tool.

**Note 1:** Only functions that are defined inside active project folders will be indexed and reachable to the lookup tool. Because of this, certain frameworks will likely have many "core" functions missing as these files are often excluded from a user's project folders.

**Note 2:** Only certain programming languages are supported so far. You may add more to the settings file yourself, but this is still somehwat experimental. If it works, it works.

**Note 3:** The more files and syntaxes you add to WhoCalled, the heavier both the indexer and lookup tool will become (indexing a whole Drupal site with custom modules works quite well though). Use freely, try its limits, have fun - but don't push your luck :).

**Note 4:** Since the plugin API is a bit different between Sublime Text 2 and Sublime Text 3, the former has no good way to show views when an item is highlighted in the quick panel. Therefore this feature is - sadly - only available to Sublime Text 3 users.

Getting started
---------------

See **Usage** below. Do the following steps, in the following order:

1. Adding project folders
2. Manually running the indexer
3. Basic

Usage
-----

#### Basic
1. Place the cursor on a function definition or call.
2. Use a keyboard shortcut (eg. `alt+l` on Windows) to find all matching function definitions in that project folder.
3. Use another keyboard shortcut (eg. `ctrl+alt+l` on Windows) to find all matching function calls in that project folder.
* Placing the cursor on a line with one or many functions (but not ON a function) will list all matches to choose from.
* Sublime Text 3 users will go to a highlighted function immediately.
* Sublime Text 2 users will go to a function upon selection.

#### Automatically running the indexer
1. Simply start/restart Sublime Text and the indexer will run automatically.

#### Manually running the indexer
1. Open up the default command palette and search for "WhoCalled".
2. Select "Run indexer".

#### Adding project folders
1. Open up the default command palette and search for "WhoCalled".
2. Select "Add/remove project folders".
3. Add and remove which folders should be indexed.
4. Restart Sublime Text or manually run the indexer.

#### Adding syntaxes
1. Open the *user* settings file.
2. Add a new file extension and corresponding regular expressions.
3. Manually re-index the project folders.
4. It might work, it might not.

Features
--------

**Function definitions**  
Will list all function definitions in a project folder that corresponds to the function being selected.

**Function calls**  
Will list all function calls in a project folder that corresponds to the function being selected.

**Automatic indexing**  
Silently indexes all project folders in the background. Ignores files that have not been changed since last time indexer was run.

**Manual indexing**  
Manual indexing will force the indexer to silently index all project folders in the background. Indexes all files, no matter when they were last changed.

**Project folders specified by the user**  
Decide which project folders should be indexed.

**Syntaxes specified by the user**  
Expand the available selection of supported programming langauges (syntaxes) by adding your own.

Feedback
--------

Feedback on WhoCalled is highly appreciated. Please direct all your feedback, questions, suggestions etc to the issue tracker here:
[https://bitbucket.org/rablador/whocalled/issues](https://bitbucket.org/rablador/whocalled/issues)

About
-----

WhoCalled is created and maintained by Rablador. The code is free.