Ver 1.1.0

Added indexing per project.

This is an important change, so old existing indexes will not work anymore. To get up and running again, please follow the steps below.

You need to:
- Open the command palette
- Select index folders
- Manually run indexer

You don't need to:
- Update any settings, they have been kept

Feedback is always welcome here:
https://bitbucket.org/rablador/whocalled/issues